# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 10:16:55 2018

@author: Anurag
"""

def re_try(fc):
    for x in range(0, 4):  # try 4 times
        try:
            temp= fc
            try:
                if('status' in list(temp.keys())):
                    if(temp['status']=='success'):
                        break
                    else:
                        time.sleep(1)
                        continue
            except:
                pass
            e= None
        except Exception as e:
            print(e)
            time.sleep(2)
            pass
    return(temp)


def get_token(user,password):
    payload= '{"username":"'+user+'","password":"'+password+'"}'
    headers= {'content-type': "application/json"}
    response= re_try(requests.post("http://testapi.climate-connect.com/api/get-token",data = payload,headers=headers).json())
    if(response['status']=='success'):
        token= response['access_token']
    else:
        print('Failure in retrieving token for API')
        token= ''
    return(token)


def get_clients(token):
    header= {"token":token ,"content-type":"application/json"}
    response= re_try(requests.post("http://testapi.climate-connect.com/api/Prism/getClients",headers=header).json())
    if(response['status']=='success'):
        temp= pd.DataFrame(response['data'])
    else:
        print('Failure in retrieving Clients Info')
        temp= pd.DataFrame()
    return(temp)

     
def get_plants(token,client_id):
    header= {"token":token ,"content-type":"application/json"}
    response= re_try(requests.post("http://testapi.climate-connect.com/api/solar/getPlant?type=plant&client_id=%s" % client_id,headers=header).json())
    if(response['status']=='success'):
        temp= pd.DataFrame(response['data'])
        temp= temp[['plantId','name','capacity','latitude','longitude','tilt','azimuth']].fillna(value=np.nan)
    else:
        print('Failure in retrieving Plants Info for the Selected Client using getPlants API')
        temp= pd.DataFrame()
    return(temp)


def get_projects(token,client_id):
    header= {"token":token ,"content-type":"application/json"}
    response= re_try(requests.post("http://testapi.climate-connect.com/api/Prism/getProjects?clientId=%s" % client_id,headers=header).json())
    if(response['status']=='success'):
        temp= pd.DataFrame(response['data'])
        temp= temp[temp.type=='weather'].sort_values('id').reset_index(drop=True).rename(columns={'id': 'plantId'})
        temp= temp[['plantId','name','capacity','latitude','longitude','tilt','azimuth']].fillna(value=np.nan)
    else:
        print('Failure in retrieving Plants Info for the Selected Client using getProjects API')
        temp= pd.DataFrame()
    return(temp)


def get_plant(plant_id,temp):
    temp= temp[temp.plantId==plant_id].copy()
    temp.reset_index(drop=True,inplace=True)
    global capacity, tilt
    capacity = temp['capacity'][0]
    tilt= temp['tilt'][0]
    tilt= tilt if np.isnan(tilt)==False else temp['latitude'][0] ### if tilt angle is not given take latitude of place as replacement
    print(get_data_date(plant_id))
    return(temp)


def get_data_date(plant_id):
    header= {"token":token ,"content-type":"application/json"}
    url= ("http://testapi.climate-connect.com/api/solar/getDataDate?client_id={0}&plantId={1}").format(client_id,plant_id)
    response= re_try(requests.post(url,headers=header).json())
    if(response['status']=='success'):   
        response= response['data']
        if(len(response)==0):
            msg='No Data Available for the plant'
        elif(all("power" not in s for s in list(response.keys()))):
            print('No Generation Data Available for the plant')
        elif(all("ghi" not in s for s in list(response.keys())) & all("gti" not in s for s in list(response.keys()))):
            msg= 'No Irradiance (both ghi and gti) Data Available for the plant'
        else:
            temp= pd.DataFrame.from_dict(response,orient='index',columns=['datetime'])
            temp['date']= temp['datetime'].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d %H:%M:%S').date())
            global start_train
            start_train= max(temp['date']) 
            del temp
            msg= ('Plant Data is Available: start_train: {0}').format(start_train)
    else:
        msg='Failure in retrieving start_train date for the plant'
    return(msg)


def get_tz(lat,long):
    try:
        url="http://api.geonames.org/timezoneJSON?formatted=true&lat="+str(lat)+"&lng="+str(long)+"&username="+'manish.sharma'+"&style=full"
        tz= re_try(requests.get(url).json())['timezoneId']
    except:
        tf = TimezoneFinder()
        tz= re_try(tf.certain_timezone_at(lng=long, lat=lat))
    global p_tz_offset,p_state
    p_tz_offset= dt.datetime.now(timezone(tz)).strftime('%z')
    p_tz_offset= p_tz_offset[:3] + ':' + p_tz_offset[3:]
    try:
        p_state= Geocoder.reverse_geocode(lat,long).administrative_area_level_1
    except:
        print('Referring plant state_name from nominatim')
        url="https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat={0}&lon={1}".format(lat,long)
        p_state= re_try(requests.get(url).json())['address']['state']
    return(tz)


def m_read(query):
    if('Accuracy' in query):
        c_id= client_id_mysql
    else:
        c_id= client_id
    payload= '{"client_id":"'+str(c_id)+'"}'
    header= {"token":token ,"content-type":"application/json"}
    try:
        response= requests.post(url= query,headers= header,data=payload).json()
        if(response['message']=='success'):
            response= response['data']
            if(len(response)!=0):
                if('generation' in query):
                    temp= pd.DataFrame.from_dict(response,orient='index').T
                    temp1= pd.melt(temp)
                    temp2= temp1['value'].apply(pd.Series)
                    temp3= pd.DataFrame({'datetime':temp2[0],'value':temp2[2],'tag':temp1['variable'],'inserted_at':temp2[1]}).dropna(subset=['datetime','value','tag'])
                    temp3['datetime']= pd.to_datetime(temp3['datetime'])
                    temp3['datetime']= temp3['datetime'].apply(lambda x: x.tz_localize('UTC').tz_convert(p_tz).tz_localize(None))
                    temp3['inserted_at']= pd.to_datetime(temp3['inserted_at'])
                    temp3['inserted_at']= temp3['inserted_at'].apply(lambda x: x.tz_localize('UTC').tz_convert(p_tz).tz_localize(None))
                elif('solar/getForecast' in query):
                    temp3= pd.DataFrame()
                    for k in response.keys():
                        temp= pd.DataFrame(response[k])
                        temp['revision']= int(k[1:])
                        temp3= temp3.append(temp,ignore_index=True)
                else:
                    temp3= pd.DataFrame(response)
            else:
                temp3= pd.DataFrame()
        else:
            print(response['message'])
    except Exception as e:
        print(e)
        temp3= pd.DataFrame()
    return(temp3)


def m_write(temp,call_type):
    for i in range(len(temp.columns)):
        if temp[temp.columns[i]].dtypes=='<M8[ns]':
            temp[temp.columns[i]]= temp[temp.columns[i]].apply(lambda x: dt.datetime.strftime(x,'%Y-%m-%d %H:%M:%S'))

    if('accuracy' in call_type):
        c_id= client_id_mysql
    else:
        c_id= client_id

    temp= temp.to_json(orient='records')
    url= calls(call_type)    
    payload= {'client_id': str(c_id), 'data': temp}
    header= {"token":token ,"content-type":"application/json"}           
    response = requests.post(url, data=json.dumps(payload), headers=header)
    return(print(response.text))


def api_regulator(call_type,plant,s_date,e_date,revision=None):
    api_limit= 30
    d_seq= pd.Series(pd.date_range(start=s_date, end=e_date, freq=str(api_limit)+'D')).apply(lambda x: x.date())
    temp1= pd.DataFrame()
    for i in range(len(d_seq)):
        t1= d_seq[i]
        t2= t1 + dt.timedelta(api_limit - 1)
        if(t2> e_date):
            t2= e_date
        temp= m_read(calls(call_type,plant,t1,t2,revision=None))
        if((len(temp)==0) and (call_type=='site')):
            temp= pd.DataFrame()
            api_limit_temp= math.floor(api_limit/2)
            while((len(temp)==0) and (api_limit_temp>=1)):
                d_seq_temp= pd.Series(pd.date_range(start=t1, end=t2, freq=str(api_limit_temp)+'D')).apply(lambda x: x.date())
                for j in range(len(d_seq_temp)):
                    t1_temp= d_seq_temp[j]
                    t2_temp= t1_temp + dt.timedelta(api_limit_temp - 1)
                    temp2= m_read(calls(call_type,plant,t1_temp,t2_temp))
                    if(len(temp2)!=0):
                        t1= t2_temp + dt.timedelta(days=1)
                    if(len(temp2)==0):
                        break
                    temp= temp.append(temp2,ignore_index=True)
                api_limit_temp= math.floor(api_limit_temp/2)
        if(len(temp)==0):
            continue
        temp1= temp1.append(temp,ignore_index=True)
    return(temp1)


def calls(call_type,plant=None,t1=None,t2=None,revision=None):
    if(call_type=='site'):
        query= ("http://testapi.climate-connect.com/api/solar/getData?type=generation&fromdate={0}&todate={1}&plantId={2}").format(t1,t2,plant)
    elif(call_type=='w_darksky'):
        query= ("http://testapi.climate-connect.com/api/weather/getDarkskyHistorical?plant-id={0}&to-date={1}&from-date={2}").format(plant,t2,t1)
    elif(call_type=='w_underground'):
        query= ("http://testapi.climate-connect.com/api/weather/getWundergroundForecast?plant-id={0}&to-date={1}&from-date={2}").format(plant,t2,t1)
    elif(call_type=='w_darksky_new'):
        query= ("http://testapi.climate-connect.com/api/weather/getForecast?source=darksky&plant-id={0}&to-date={1}&from-date={2}").format(plant,t2,t1)
    elif(call_type=='solar_parameter'):
        query= ("http://testapi.climate-connect.com/api/solar/getSolarParameter?fromdate={0}&todate={1}&plantId={2}").format(t1,t2,plant)
    elif(call_type=='set_ghi_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/setForecast?type=ghi")
    elif(call_type=='set_gti_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/setForecast?type=gti")
    elif(call_type=='set_gen_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/setForecast?type=gen")
    elif(call_type=='get_full_ghi_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=ghi&date={0}&plantId={1}").format(t1,plant)
    elif(call_type=='get_full_gti_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=gti&date={0}&plantId={1}").format(t1,plant)
    elif(call_type=='get_full_gen_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=gen&date={0}&plantId={1}").format(t1,plant)
    elif(call_type=='get_ghi_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=ghi&date={0}&plantId={1}&revision={2}").format(t1,plant,'R'+str(revision))
    elif(call_type=='get_gti_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=gti&date={0}&plantId={1}&revision={2}").format(t1,plant,'R'+str(revision))
    elif(call_type=='get_gen_forecast'):
        query= ("http://testapi.climate-connect.com/api/solar/getForecast?type=gen&date={0}&plantId={1}&revision={2}").format(t1,plant,'R'+str(revision))
    elif(call_type=='get_plant_accuracy'):
        query= ("http://testapi.climate-connect.com/api/solar/getAccuracy?type=plant&fromdate={0}&todate={1}&plantId={2}").format(t1,t2,plant)
    elif(call_type=='get_cum_accuracy'):
        query= ("http://testapi.climate-connect.com/api/solar/getAccuracy?type=cumulative&fromdate={0}&todate={1}&plantId={2}").format(t1,t2,plant)
    elif(call_type=='set_plant_accuracy'):
        query= ("http://testapi.climate-connect.com/api/solar/setAccuracy?type=plant")
    elif(call_type=='set_cum_accuracy'):
        query= ("http://testapi.climate-connect.com/api/solar/setAccuracy?type=cumulative")
    else:
        print("call the api properly")
    return query