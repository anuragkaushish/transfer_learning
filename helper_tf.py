# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 19:16:51 2018

@author: Anurag
"""

####### Weather data #######
def weather_data_loader(plant_id,t_date):
    data= api_regulator('w_darksky',plant_id,t_date,t_date)
    if(len(data)==0):
        data= api_regulator('w_darksky',plant_id,t_date,t_date)
        if(len(data)==0):
            data= api_regulator('w_darksky_new',plant_id,t_date,t_date)
    new_data= data[['datetime_local','cloud_cover','apparent_temperature','humidity','wind_speed','wind_bearing']].copy()
    new_data.rename(columns={'datetime_local': 'datetime','humidity': 'relative_humidity','apparent_temperature': 'apparent_temp','wind_bearing': 'wind_dir'},inplace=True)
    new_data['datetime'] = new_data['datetime'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
    new_data['datetime'] = new_data['datetime'].apply(lambda x: x.floor('min'))
    new_data['datetime'] = new_data['datetime'].dt.floor(str(b_size)+'min')
    new_data[new_data== 'Null']= np.nan
    for i in range(1,len(new_data.columns)):
        if new_data[new_data.columns[i]].dtypes=='O':
            new_data[new_data.columns[i]]= new_data[new_data.columns[i]].fillna(np.nan).astype('float')
    
    new_data = new_data.groupby('datetime').mean().reset_index()
    all_datetime = pd.Series(pd.date_range(start = t_date,end = t_date+dt.timedelta(days=1),freq=str(b_size)+'T'))[:-1]
    date_interval = all_datetime.to_frame().rename(columns = {0:'datetime'})
    new_data = pd.merge(date_interval,new_data,on='datetime',how='outer')
    new_data = new_data.interpolate(method = 'linear')
    new_data= new_data.fillna(method='ffill')
    new_data= new_data.fillna(method='bfill')
    new_data['date'] = new_data['datetime'].apply(lambda x: x.date())    
    new_data['time'] = new_data['datetime'].apply(lambda x: x.time())
    new_data['time_block'] = new_data['datetime'].apply(lambda x: int(((x.hour*60)+x.minute)/b_size+1))
    new_data.sort_values(['time_block','date'],inplace=True)
    new_data.reset_index(drop=True,inplace = True)    
    new_data = new_data[['datetime', 'date', 'time', 'time_block','cloud_cover','apparent_temp','relative_humidity','wind_speed','wind_dir']]
    return new_data    


####### solar parameter ######
def solpos_loader(plant_id,t_date):
    new_data= api_regulator('solar_parameter',plant_id,t_date,t_date)[['local_datetime','zenith','etr','azimuthal']].rename(columns={'local_datetime': 'datetime'})
    new_data['datetime'] = new_data['datetime'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
    new_data['datetime'] = new_data['datetime'].apply(lambda x: x.floor('min'))
    new_data['datetime'] = new_data['datetime'].dt.floor(str(b_size)+'min')
    new_data = new_data.groupby('datetime').mean().reset_index()
    all_datetime = pd.Series(pd.date_range(start = t_date,end = t_date+dt.timedelta(days=1),freq=str(b_size)+'T'))[:-1]
    date_interval = all_datetime.to_frame().rename(columns = {0:'datetime'})
    new_data = pd.merge(date_interval,new_data,on='datetime',how='outer')
    new_data = new_data.interpolate(method = 'linear')
    new_data= new_data.fillna(method='ffill')
    new_data= new_data.fillna(method='bfill')
    new_data['date'] = new_data['datetime'].apply(lambda x: x.date())    
    new_data['time'] = new_data['datetime'].apply(lambda x: x.time())
    new_data['time_block'] = new_data['datetime'].apply(lambda x: int(((x.hour*60)+x.minute)/15+1))
    new_data= new_data[new_data.date==t_date]
    new_data.sort_values(['time_block','date'],inplace=True)
    new_data.reset_index(drop=True,inplace = True)
    new_data= new_data[['datetime', 'date', 'time', 'time_block', 'zenith', 'azimuthal', 'etr']]
    return new_data


#### Max Revised generation forecast
def mrG_forecast(plant_id, t_date):
    new_data= api_regulator('get_full_gen_forecast',plant_id,t_date,t_date)
    global max_revision
    if(len(new_data)!=0):
        new_data=new_data.rename(columns={'local_datetime': 'datetime','forecast': 'generation'})
        max_revision= np.nanmax(new_data['revision'])
        new_data= new_data[new_data['implementedTS'].isnull()==False]
        new_data.drop('implementedTS',axis=1,inplace=True)
        if(len(new_data) != 0):
            new_data = new_data[new_data['revision'] == np.nanmax(new_data['revision'])]    
        new_data['datetime'] = new_data['datetime'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
        new_data['createdTS'] = new_data['createdTS'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
        new_data['createdTS'] = new_data['createdTS'].apply(lambda x: x.tz_localize('UTC').tz_convert(p_tz).tz_localize(None))
        new_data['date'] = new_data['datetime'].apply(lambda x: x.date())    
        new_data['time'] = new_data['datetime'].apply(lambda x: x.time())
        new_data['time_block'] = new_data['datetime'].apply(lambda x: int(((x.hour*60)+x.minute)/15+1))
        new_data.sort_values('datetime',inplace=True)
        new_data.reset_index(drop=True,inplace=True)
        new_data= new_data[['datetime', 'date', 'time', 'time_block', 'generation', 'revision','createdTS']]
    else:
        new_data= pd.DataFrame(columns=['datetime', 'date', 'time', 'time_block', 'generation', 'revision','createdTS'])
        max_revision= 0
    return new_data


#### Live  Generation Data from Scada or FTP###
def live_GF(plant_id, t_date):
    temp= api_regulator('site',plant_id,t_date,t_date)
    new_data= temp[temp['tag'].str.contains('p')].copy().rename(columns={'value': 'gen_A'})
    new_data.drop('inserted_at',axis=1,inplace=True)
    
    new_data['datetime'] = new_data['datetime'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
    new_data = new_data[new_data['gen_A'] >= 0]
#    new_data.loc[((new_data['gen_A']>capacity) & (new_data['gen_A']< (capacity+5))),'gen_A']= capacity
#    new_data = new_data[new_data['gen_A'] <= capacity]
    new_data = new_data.groupby('datetime').sum().reset_index()
    new_data['datetime'] = new_data['datetime'].dt.floor(str(b_size)+'min')
    new_data = new_data.groupby('datetime').mean().reset_index()
    new_data['gen_A'] = new_data['gen_A'].apply(lambda x: round(x,2))
    new_data.loc[new_data.gen_A==0,'gen_A'] = np.nan
    new_data['date'] = new_data['datetime'].apply(lambda x: x.date())    
    new_data['time'] = new_data['datetime'].apply(lambda x: x.time())
    new_data['time_block'] = new_data['datetime'].apply(lambda x: int(((x.hour*60)+x.minute)/b_size+1))
    new_data= new_data[new_data.date==t_date]
    new_data.sort_values('datetime',inplace=True)
    new_data.reset_index(drop=True,inplace=True)
    new_data= new_data[['datetime', 'date', 'time', 'time_block', 'gen_A']]
    return new_data


def report_generator(temp):
    file_name = 'revision_0_schedule_2018-07-09_Banavikal.xls'
    df_excel = pd.read_excel(io=file_name,header=None)
    if(plant_id==1):
        df_excel.iloc[0,2]= 'ACME Karnal Solar Power Pvt. Ltd. Galiveedu - Plot 1'
    elif(plant_id==2):
        df_excel.iloc[0,2]= 'ACME Hisar Solar Power Pvt. Ltd. Galiveedu - PLot 2'
    elif(plant_id==3):
        df_excel.iloc[0,2]= 'ACME Bhiwadi Solar Power Pvt. Ltd. Galiveedu - Plot 3'    
    df_excel.iloc[2,2]= '50 MW'
    df_excel.iloc[1,2]= test_date
    
    df_excel.iloc[3,2]= temp['revision'][0]
    df_excel.loc[12:108,0]= test_date
    df_excel.loc[12:108,4]= temp['generation'].values
    df_excel.loc[12:108,3]= capacity
    global r_name
    r_name= 'revision_{0}_schedule_{1}_{2}'.format(temp['revision'][0],test_date,df_excel.iloc[0,2])
    return(df_excel)