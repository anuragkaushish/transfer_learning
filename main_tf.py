# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 18:23:55 2018

@author: Anurag
"""

### Transfer Learning Main Script: oth for Day-Ahead and Intra-Day Forecasting
### No plant historical data required
 
import os, sys
#path="D:/climate connect/transfer_learning/"
path= os.path.abspath(os.path.dirname(sys.argv[0]))
os.chdir(path)

exec(open('./setup.py').read())
exec(open('./api_cred.py').read())
exec(open('./api_fun.py').read())
exec(open('./helper_tf.py').read())
#exec(open('./m_mailer.py').read())

### API: Token Key
token = get_token(user,password)

### Client's Names
clients= get_clients(token)
#print(clients)

### Choose Client Name to Work on
client_id= clients.loc[clients.name=='Amplus','clientId'].item()
print('client_id: '+str(client_id))

### All plants info for the selected client
plants_info= get_projects(token,client_id)
if(len(plants_info)==0):
    print('Checking in getProjects for plants info')
    plants_info=  get_plants(token,client_id)

### Default ppa Value: change as per state and client rules
ppa=5
b_size=15   ### Block Size(# of Minutes)

### Empty dataframes for storing results for local system
dts = pd.DataFrame()
accuracy= pd.DataFrame()

### Load the trained model and scaler
mod_name= 'm_model_svm_2018-07-06.sav'
mod = pickle.load(open(mod_name, 'rb'))
scale= joblib.load('m_scale_transform_2018-07-06.pkl')

### real = True means real time simulation and False means Backcast
real = True
blocks= 45,92,2,10,14,20,26,32,38,44,50,56,62,68,74,80,86]
load_ratio= 175/135

for plant_id in plants_info['plantId'].tolist():
    try:
        print('plant_id= {0}'.format(plant_id))
        plant= get_plant(plant_id,plants_info)    
        p_tz= get_tz(plant['latitude'][0],plant['longitude'][0])
        os.environ['TZ'] = p_tz
        print('p_state= {0}'.format(p_state))
        print("p_tz_offset= '{0}'".format(p_tz_offset)) ### plant timezone offset w.r.t. to UTC
        print("runtime: %s" % str(dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        
        ### Specify start test and end test dates
        start_test= dt.datetime.now().date()  #- dt.timedelta(days = 1)
        end_test= dt.datetime.now().date()  #- dt.timedelta(days = 1)
        test_int= pd.Series(pd.date_range(start_test,end_test)).apply(lambda x: x.date())
        
        c_time= dt.datetime.now().replace(microsecond=0)   ### Current Time
        c_block= int(((c_time.hour*60)+c_time.minute)/b_size+1)   ### Current Time Block
        print(c_block)

        for t in range(len(test_int)):
            if c_block in blocks:
#            for c_block in blocks:
                if(c_block in [21]):
                    k_values=[1]
                else:
                    k_values=[0]
                print(c_block)
                for k in k_values:
                    test_date= test_int[t] + dt.timedelta(days=k)
                    print('forecast_date: {0}'.format(test_date))
            
                    weather_data= weather_data_loader(plant_id,test_date)
                    solpos_data= solpos_loader(plant_id,test_date)
                    test= pd.merge(weather_data,solpos_data,on=['datetime','date','time','time_block'],how='inner')
                    test_temp= scale.transform(test[['cloud_cover','apparent_temp','relative_humidity','wind_speed',
                                                     'wind_dir','zenith','azimuthal','etr']])
                    
                    test['generation'] = np.round(mod.predict(test_temp),2)*capacity*load_ratio
                    test['generation']= test['generation'] - np.random.uniform(0, 0.05, (96, ))
                    test.loc[test['generation']<0,'generation']= 0
                    test.loc[test['generation']>capacity,'generation']= capacity
                    test.loc[(test['time_block']<=20) | (test['time_block']>=80),'generation']= 0
                    test['generation']= test['generation'].apply(lambda x: round(x,2))
                    
                    test= pd.merge(test,live_GF(plant_id,test_date),on=['datetime','date','time','time_block'],how='outer')
                     test['accuracy']= (1-abs(test['gen_A']-test['generation'])/capacity)*100
                    
                    mrGf= mrG_forecast(plant_id,test_date)   
                    if(len(mrGf)==0):
                        test['revision']= 0
                    else:
                        test['revision']= max_revision+1
            
                    if(real==False):
                        test['createdTS']= test.loc[test['time_block']==(c_block+1),'datetime'][c_block]
                    else:
                        test['createdTS']= dt.datetime.now().replace(microsecond=0)
                    test['createdTS']= test['createdTS'].apply(lambda x: x.tz_localize(p_tz).tz_convert('UTC').tz_localize(None))
                    test['implementedTS']= test['createdTS']
                    if(test['revision'][0]==0):
                        test['implemented']= 'NULL'
                    else:
                        test['implemented']= 'YES'
                    test['local_datetime']= test['datetime'].iloc[0:]
                    test['datetime']= test['datetime'].apply(lambda x: x.tz_localize(p_tz).tz_convert('UTC').tz_localize(None))
                    test['plant_id']= plant_id
                    
                    df= test[['plant_id','datetime','local_datetime','revision','generation',
                              'createdTS','implementedTS','implemented','time_block']].copy()
                    
                    if((max_revision+1)>1):
                        df.loc[df['time_block']<=(c_block+2),'generation']= mrGf.loc[mrGf['time_block']<=(c_block+2),'generation']
                        df['implemented']= np.repeat(['NULL','YES'], [(c_block + 2), ((1440/b_size) - c_block - 2)], axis = 0)
      
                        u_mrGf= api_regulator('get_full_gen_forecast',plant_id,test_date,test_date).rename(columns={'forecast': 'generation','utc_datetime': 'datetime'})
                        u_mrGf['local_datetime'] = u_mrGf['local_datetime'].apply(lambda x: dt.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
                        u_mrGf['time_block'] = u_mrGf['local_datetime'].apply(lambda x: int(((x.hour*60)+x.minute)/15+1))
                        u_mrGf.loc[u_mrGf['time_block']> c_block+2,'implemented'] = 'Null'
                        u_mrGf.drop(['time_block'], axis = 1, inplace = True)
                        u_mrGf['plant_id']= plant_id
                        print(m_write(u_mrGf.copy(),'set_gen_forecast'))
                        print('previous revisions status updated successfully')
                    
                    df.drop(['time_block'], axis = 1, inplace = True)
                    print('generation forecast ready: time for connection to db')
                    print(m_write(df.copy(),'set_gen_forecast'))
                    print('generation forecast uploaded successfully')
                    
#                    temp= pd.DataFrame({'plant_id': plant_id,'date': test_date,'revision': revision,
#                                        'accuracy': np.round(np.nanmean(test['accuracy']),2)})
#                    accuracy= accuracy.append(temp)
                    
                    dts= dts.append(test)
                    
#                    df_excel= report_generator(df.copy()) ### formatting dataframe
#                    df_excel.to_excel(r_name+'.xls',index=False,header=False)  ### excel sheet generator
#                    
#                    sender= "scheduling@climate-connect.com"
#                    recipient= ["acme-ap@climate-connect.com"]
#                    subject= r_name
#                    send_time= dt.datetime.now().strftime('%H:%M')
#                    if(df['revision'][0] in [0,1]):
#                        imp_time= '00:00'
#                    else:
#                        imp_time= (pd.Series(dt.datetime.now()).dt.floor(str(b_size)+'min')[0] + dt.timedelta(minutes=45)).strftime('%H:%M')
#                    body= ("Send Date {0}\nSend Time {1} Hrs\nRevision no. : {2}\n"
#                           "Effective from : {3} Hrs").format(test_int[t],send_time,df['revision'][0],imp_time)
#                    
#                    attachment= m_attach(r_name+'.xls')   ### create attachment
#                    
#                    print('Initiating Revision Mailing')
#                    print(m_mail(sender, recipient, subject, body, attachment))
#                    print('Completed Revision Mailing')
    except:
        pass