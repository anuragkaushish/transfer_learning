# -*- coding: utf-8 -*-
"""
Created on Tue May 22 11:20:43 2018

@author: Anurag
"""

# Setup
import sys, os
import datetime as dt
import pandas as pd
import numpy as np
import pymysql
import datetime as dt
import time
import math
from sqlalchemy.engine import create_engine
from pytz import timezone
import matplotlib.pyplot as plt
from scipy import stats
#import fancyimpute
from scipy.spatial.distance import mahalanobis
from scipy.stats import zscore
import random
import warnings
import requests
import json
import subprocess
from functools import reduce
from sklearn import svm, metrics
from sklearn.model_selection import ParameterGrid, GridSearchCV
from sklearn.preprocessing import MinMaxScaler
from timezonefinder import TimezoneFinder
from pygeocoder import Geocoder
from ast import literal_eval
import pickle
from sklearn.externals import joblib
from mimetypes import MimeTypes